﻿using System;
using System.Collections.Generic;
using System.Text;
using laba_10;
using laba_13;
namespace laba_14
{
    public static class MyNewCollectionUpgrade
    {
        public static List<string> MagazineNames(this MyNewCollection<PrintedEdition> queue)
        {
            List<string> result = new List<string>();
            foreach (var i in queue) if (i is Magazine) result.Add(i.Name);
            return result;
        }
        public static List<PrintedEdition> Intersection(this MyNewCollection<PrintedEdition> queue, MyNewCollection<PrintedEdition> q)
        {
            List<PrintedEdition> result = new List<PrintedEdition>();
            foreach (var i in queue)
                {
                    foreach (var k in q)
                        {
                            if (i == k) result.Add(i.Clone() as PrintedEdition);
                        }
                }
            return result;
        }
        public static int LowPrice(this MyNewCollection<PrintedEdition> queue, int p)
        {
            int result = 0;
            foreach (var i in queue) if (i.Price < p) result++;
            return result;
        }
    }
}
