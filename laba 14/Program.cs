﻿using System;
using System.Collections.Generic;
using System.Linq;
using InputLib;
using laba_10;
using laba_13;
namespace laba_14
{
    public class Program
    {
        static public List<string> MagazineNamesLINQ(Queue<Queue<PrintedEdition>> q)
        {
            var r = from grouppe in q from pe in grouppe where pe is Magazine select pe.Name;
            return r.ToList();
        }
        static public List<PrintedEdition> IntersectionLINQ(Queue<Queue<PrintedEdition>> q1, Queue<Queue<PrintedEdition>> q2)
        {
            var r = (from grouppe in q1 from pe in grouppe select pe).Intersect(from grouppe2 in q2 from pe2 in grouppe2 select pe2);
            return r.ToList();
        }
        static public int LowPriceLINQ(Queue<Queue<PrintedEdition>> q,int p)
        {
            return (from grouppe in q from pe in grouppe where pe.Price<p select pe).Count();
        }
        static public double BookPriceLINQ(Queue<Queue<PrintedEdition>> q)
        {
            return (from grouppe in q from pe in grouppe where pe is Book select pe.Price).Average();
        }
        static public Dictionary<string,List<PrintedEdition>> GroupTypeLINQ(Queue<Queue<PrintedEdition>> q)
        {
            var data = from grouppe in q from pe in grouppe group pe by pe.GetType();
            Dictionary<string, List<PrintedEdition>> result = new Dictionary<string, List<PrintedEdition>>();
            foreach (var i in data) result.Add(i.Key.ToString(), i.ToList());
            return result;
        }
        static void Main(string[] args)
        {
            Queue<Queue<PrintedEdition>> queue1 = new Queue<Queue<PrintedEdition>>();
            Queue<PrintedEdition> Q1 = new Queue<PrintedEdition>();
            Q1.Enqueue(new Book(2002, "A1", "N1", 1000, 2));
            Q1.Enqueue(new Book(2003, "A2", "N2", 1200, 1));
            Queue<PrintedEdition> Q2 = new Queue<PrintedEdition>();
            Q1.Enqueue(new Magazine(2004, "A3", "N3", 300, 10));
            Q1.Enqueue(new TextBook(2010, "A4", "N4", 2000, 2, "S1"));
            queue1.Enqueue(Q1);
            queue1.Enqueue(Q2);

            Queue<Queue<PrintedEdition>> helpqueue1 = new Queue<Queue<PrintedEdition>>();
            Queue<PrintedEdition> Q3 = new Queue<PrintedEdition>();
            Q3.Enqueue(new Book(2002, "A1", "N1", 1000, 2));
            Q3.Enqueue(new Book(2015, "A5", "N5", 1300, 1));
            Queue<PrintedEdition> Q4 = new Queue<PrintedEdition>();
            Q4.Enqueue(new Magazine(2001, "A6", "N6", 200, 8));
            Q4.Enqueue(new TextBook(2010, "A4", "N4", 2000, 2, "S1"));
            helpqueue1.Enqueue(Q3);
            helpqueue1.Enqueue(Q4);
            
            /*
            int Qlen1 = 2;
            for (int i = 0; i < Qlen1; i++)
            {
                Queue<PrintedEdition> Q = new Queue<PrintedEdition>();
                for (int k = 0; k < Qlen1; k++) Q.Enqueue(RandomFactory.GetRandom(-1));
                queue1.Enqueue(Q);
            }

            int Qlen2 = 3;
            for (int i = 0; i < Qlen2; i++)
            {
                queue2.Add(RandomFactory.GetRandom(-1));
            }
            */
            MyNewCollection<PrintedEdition> queue2 = new MyNewCollection<PrintedEdition>();
            queue2.Add(new Book(2000, "A1", "N1", 1000, 1));
            queue2.Add(new Book(2001, "A2", "N2", 1500, 2));
            queue2.Add(new Magazine(2000, "A3", "N3", 300, 10));
            MyNewCollection<PrintedEdition> helpqueue2 = new MyNewCollection<PrintedEdition>();
            helpqueue2.Add(new Book(2000, "A1", "N1", 1000, 1));
            helpqueue2.Add(new Book(2021, "A4", "N4", 3000, 1));
            helpqueue2.Add(new Magazine(2000, "A3", "N3", 300, 10));

            Console.WriteLine("=====QUEUE=====");
            foreach (var i in queue1) foreach (PrintedEdition k in i) k.ShowObject();

            Console.WriteLine("=====QUEUE===== [вспомогательная коллекция]");
            foreach (var i in helpqueue1) foreach (PrintedEdition k in i) k.ShowObject();

            Console.WriteLine("=====MYNEWCOLLECTION=====");
            foreach (PrintedEdition i in queue2) i.ShowObject();

            Console.WriteLine("=====MYNEWCOLLECTION===== [вспомогательная коллекция]");
            foreach (PrintedEdition i in helpqueue2) i.ShowObject();

            while (true)
            {
                Console.WriteLine("+-----------------------------------------+");
                Console.WriteLine("|             Запросы к queue             |");
                Console.WriteLine("+-----------------------------------------+");
                Console.WriteLine("|1 Наименования журналов                  |");
                Console.WriteLine("|2 Кол-во изданий с ценой, меньше заданной|");
                Console.WriteLine("|3 пересечение со второй коллекцией       |");
                Console.WriteLine("|4 Средняя цена книги                     |");
                Console.WriteLine("|5 Группировка по типу                    |");
                Console.WriteLine("+-----------------------------------------+");
                Console.WriteLine("|        Запросы к mynewcollection        |");
                Console.WriteLine("+-----------------------------------------+");
                Console.WriteLine("|6 Наименования журналов                  |");
                Console.WriteLine("|7 Кол-во изданий с ценой, меньше заданной|");
                Console.WriteLine("|8 пересечение со второй коллекцией       |");
                Console.WriteLine("+-----------------------------------------+");
                int command = Stream.Input("", 1, 8);
                if (command == 1)
                {
                    Console.WriteLine("Ответ от Linq");

                    List<string> result1 = MagazineNamesLINQ(queue1);
                    foreach (var i in result1) Console.WriteLine(i);

                    Console.WriteLine("Ответ от метода расширения");

                    List<string> result2 = queue1.MagazineNames();
                    foreach (var i in result2) Console.WriteLine(i);
                }
                else if (command == 2)
                {
                    int price = Stream.Input("Введите цену ", 1, 100000);
                    Console.WriteLine("Ответ от Linq");

                    Console.WriteLine(LowPriceLINQ(queue1, price));

                    Console.WriteLine("Ответ от метода расширения");

                    Console.WriteLine(queue1.LowPrice(price));
                }
                else if (command == 3)
                {
                    Console.WriteLine("Ответ от Linq");

                    List<PrintedEdition> result1 = IntersectionLINQ(queue1, helpqueue1);
                    foreach (var i in result1) i.ShowObject();

                    Console.WriteLine("Ответ от метода расширения");

                    List<PrintedEdition> result2 = queue1.Intersection(helpqueue1);
                    foreach (var i in result2) i.ShowObject();
                }
                if (command == 4)
                {
                    Console.WriteLine("Ответ от Linq");

                    Console.WriteLine(BookPriceLINQ(queue1));

                    Console.WriteLine("Ответ от метода расширения");

                    Console.WriteLine(queue1.BookPrice());
                }
                if (command == 5)
                {
                    Console.WriteLine("Ответ от Linq");

                    Dictionary<string, List<PrintedEdition>> result1 = GroupTypeLINQ(queue1);
                    foreach (var i in result1)
                    {
                        Console.WriteLine(i.Key);
                        foreach (var k in i.Value) k.ShowObject();
                    }

                    Console.WriteLine("Ответ от метода расширения");

                    Dictionary<string, List<PrintedEdition>> result2 = queue1.GroupType();
                    foreach (var i in result2)
                    {
                        Console.WriteLine(i.Key);
                        foreach (var k in i.Value) k.ShowObject();
                    }
                }
                else if (command == 6)
                {
                    List<string> result2 = queue2.MagazineNames();
                    foreach (var i in result2) Console.WriteLine(i);
                }
                else if (command == 7)
                {
                    int price = Stream.Input("Введите цену ", 1, 100000);
                    Console.WriteLine(queue2.LowPrice(price));
                }
                else if (command == 8)
                {
                    List<PrintedEdition> result2 = queue2.Intersection(helpqueue2);
                    foreach (var i in result2) i.ShowObject();
                }
            }
        }
    }
}
