﻿using System;
using System.Collections.Generic;
using System.Text;
using laba_10;
namespace laba_14
{
    public static class QueueUpgrade
    {
        public static List<string> MagazineNames(this Queue<Queue<PrintedEdition>> queue)
        {
            List<string> result = new List<string>();
            foreach (var k in queue) foreach (var i in k) if (i is Magazine) result.Add(i.Name);
            return result;
        }
        public static List<PrintedEdition> Intersection(this Queue<Queue<PrintedEdition>> queue, Queue<Queue<PrintedEdition>> q)
        {
            List<PrintedEdition> result = new List<PrintedEdition>();
            foreach (var k in queue) foreach (var i in k)
            {
                foreach (var j in q) foreach (var l in j)
                {
                    if (i == l) result.Add(i.Clone() as PrintedEdition);
                }
            }
            return result;
        }
        public static int LowPrice(this Queue<Queue<PrintedEdition>> queue,int p)
        {
            int result=0;
            foreach (var k in queue) foreach (var i in k) if (i.Price<p) result++;
            return result;
        }
        public static double BookPrice(this Queue<Queue<PrintedEdition>> queue)
        {
            double count = 0;
            double sum = 0;
            foreach (var k in queue) foreach (var i in k) if (i is Book)
            {
               count++;
               sum += i.Price;
            }
            return sum/count;
        }
        public static Dictionary<string, List<PrintedEdition>> GroupType(this Queue<Queue<PrintedEdition>> q)
        {
            Dictionary<string, List<PrintedEdition>> result = new Dictionary<string, List<PrintedEdition>>();
            foreach (var k in q) foreach (var i in k)
            {
                    string tip = i.GetType().ToString();
                    if (result.ContainsKey(tip)) result[tip].Add(i);
                    else
                    {
                        List<PrintedEdition> el = new List<PrintedEdition>();
                        el.Add(i);
                        result.Add(tip, el);
                    }
            }
            return result;
        }

    }
}
