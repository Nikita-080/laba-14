using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using laba_14;
using laba_13;
using laba_10;
namespace UnitTest1
{
    [TestClass]
    public class UnitTest1
    {
        Queue<Queue<PrintedEdition>> queue1 = new Queue<Queue<PrintedEdition>>();
        Queue<Queue<PrintedEdition>> helpqueue1 = new Queue<Queue<PrintedEdition>>();
        MyNewCollection<PrintedEdition> queue2 = new MyNewCollection<PrintedEdition>();
        MyNewCollection<PrintedEdition> helpqueue2 = new MyNewCollection<PrintedEdition>();
        public UnitTest1() 
        {
            Queue<PrintedEdition> Q1 = new Queue<PrintedEdition>();
            Q1.Enqueue(new Book(2002, "A1", "N1", 1000, 2));
            Q1.Enqueue(new Book(2003, "A2", "N2", 1200, 1));
            Queue<PrintedEdition> Q2 = new Queue<PrintedEdition>();
            Q1.Enqueue(new Magazine(2004, "A3", "N3", 300, 10));
            Q1.Enqueue(new TextBook(2010, "A4", "N4", 2000, 2, "S1"));
            queue1.Enqueue(Q1);
            queue1.Enqueue(Q2);

            Queue<PrintedEdition> Q3 = new Queue<PrintedEdition>();
            Q3.Enqueue(new Book(2002, "A1", "N1", 1000, 2));
            Q3.Enqueue(new Book(2015, "A5", "N5", 1300, 1));
            Queue<PrintedEdition> Q4 = new Queue<PrintedEdition>();
            Q4.Enqueue(new Magazine(2001, "A6", "N6", 200, 8));
            Q4.Enqueue(new TextBook(2010, "A4", "N4", 2000, 2, "S2"));
            helpqueue1.Enqueue(Q3);
            helpqueue1.Enqueue(Q4);

            queue2.Add(new Book(2000, "A1", "N1", 1000, 1));
            queue2.Add(new Book(2001, "A2", "N2", 1500, 2));
            queue2.Add(new Magazine(2000, "A3", "N3", 300, 10));

            helpqueue2.Add(new Book(2000, "A1", "N1", 1000, 1));
            helpqueue2.Add(new Book(2021, "A4", "N4", 3000, 1));
            helpqueue2.Add(new Magazine(2000, "A3", "N3", 300, 11));
        }
        [TestMethod]
        public void MagazineNames_Q_EM()
        {
            string result = queue1.MagazineNames()[0];
            string trueresult = "N3";
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void MagazineNames_Q_LINQ()
        {
            string result = Program.MagazineNamesLINQ(queue1)[0];
            string trueresult = "N3";
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void LowPrice_Q_EM()
        {
            int result = queue1.LowPrice(1001);
            int trueresult = 2;
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void LowPrice_Q_LINQ()
        {
            int result = Program.LowPriceLINQ(queue1,1001);
            int trueresult = 2;
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void Intersection_Q_EM()
        {
            PrintedEdition result = queue1.Intersection(helpqueue1)[0];
            PrintedEdition trueresult = new Book(2002, "A1", "N1", 1000, 2);
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void Intersection_Q_LINQ()
        {
            PrintedEdition result = Program.IntersectionLINQ(queue1, helpqueue1)[0];
            PrintedEdition trueresult = new Book(2002, "A1", "N1", 1000, 2);
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void BookPrice_Q_EM()
        {
            double result = queue1.BookPrice();
            double trueresult = 1400;
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void BookPrice_Q_LINQ()
        {
            double result = Program.BookPriceLINQ(queue1);
            double trueresult = 1400;
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void GroupType_Q_LINQ()
        {
            Dictionary<string, List<PrintedEdition>> result1 = Program.GroupTypeLINQ(queue1);
            foreach (var i in result1) foreach (var k in i.Value) Assert.AreEqual(k.GetType().ToString(), i.Key);
        }
        [TestMethod]
        public void GroupType_Q_EM()
        {
            Dictionary<string, List<PrintedEdition>> result1 = queue1.GroupType();
            foreach (var i in result1) foreach (var k in i.Value) Assert.AreEqual(k.GetType().ToString(), i.Key);
        }
        [TestMethod]
        public void MagazineNames_MNC_EM()
        {
            string result = queue2.MagazineNames()[0];
            string trueresult = "N3";
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void LowPrice_MNC_EM()
        {
            int result = queue2.LowPrice(1001);
            int trueresult = 2;
            Assert.AreEqual(result, trueresult);
        }
        [TestMethod]
        public void Intersection_MNC_EM()
        {
            PrintedEdition result = queue2.Intersection(helpqueue2)[0];
            PrintedEdition trueresult = new Book(2000, "A1", "N1", 1000, 1);
            Assert.AreEqual(result, trueresult);
        }
    }
}
